﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_3_6
{
    class Program_zad6
    {
        
        static void Main(string[] args)
        {
            
            string author = "Denis Ivanović";
            NotificationBuilder builder = new NotificationBuilder();
            NotificationManager notificationManager = new NotificationManager();
            Director director = new Director(builder);
            ConsoleNotification notification;

            director.ConstructAlertNotification(author);
            notification = builder.Build();
            notificationManager.Display(notification);

            director.ConstructErrorNotification(author);
            notification = builder.Build();
            notificationManager.Display(notification);

            director.ConstructInfoNotification(author);
            notification = builder.Build();
            notificationManager.Display(notification);
        }
        
    }
}
