﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_3_5
{
    class Program_zad5
    {
        
        static void Main(string[] args)
        {
            NotificationBuilder builder = new NotificationBuilder();
            NotificationManager notificationManager = new NotificationManager();
            builder.SetAuthor("Denis Ivanović");
            builder.SetColor(ConsoleColor.Red);
            builder.SetLevel(Category.INFO);
            builder.SetText("Tko vjeruje ljepoti gradi na pijesku");
            builder.SetTime(DateTime.Now);
            builder.SetTitle("Njemačka narodna poslovica");
            ConsoleNotification notification = builder.Build();
            notificationManager.Display(notification);
        }
        
    }

}
