﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON___LV_3_1
{
    class Dataset:Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            IList<List<string>> data = this.data;
            return data;
        }
        public Prototype Clone()
        {
            Dataset deepcopyDataset = new Dataset();
            deepcopyDataset.data = data.Select(x => x.ToList()).ToList();
            return deepcopyDataset;
        }
        public Prototype CloneP()
        {
            return (Prototype)this.MemberwiseClone();
        }
        public void ClearData()
        {
            this.data.Clear();
        }
    }
}
