﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_3_1
{
    class Program_zad1
    {
        
        static void Main(string[] args)
        {
            /*
            Pitanje: Je li potrebno duboko kopiranje za klasu u pitanju?
            Odgovor: Mislim da nema potrebe za dubokim kopiranjem jer se podatci svakako čitaju iz datoteke, ali sve zavisi od potrebe
                     npr. ako mislimo mijenjati podatke u datoteki, a trebaju nam orginalni podatci za usporedbu npr. onda moramo izvršit duboko kopiranje
                     kako bi nam ostali orginalni podatci u kopiji nakon izmjene.
            */

            Dataset dataset1 = new Dataset();
            Dataset dataset2;
            IList<List<string>> data1 = new List<List<string>>();
            IList<List<string>> data2 = new List<List<string>>();

            dataset1.LoadDataFromCSV("data.csv");
            dataset2 = (Dataset)dataset1.Clone();

            data1 = dataset1.GetData();
            data2 = dataset2.GetData();

            foreach (List<string> list in data1)
            {
                foreach (string word in list)
                {
                    Console.Write(word + " ");
                }
            }
            Console.WriteLine();
            foreach (List<string> list in data2)
                {
                    foreach (string word in list)
                    {
                        Console.Write(word + " ");
                    }
                }
        }
        
    }
}
