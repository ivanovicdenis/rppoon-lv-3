﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Enumeration;
using System.Runtime.CompilerServices;
using System.Text;

namespace RPPOON___LV_3_3
{
    class Logger
    {
        private StreamWriter writer;
        private static Logger logger;
        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
        private Logger(string fileName)
        {
            this.FileName = fileName;
        }
        public static Logger GetInstance()
        {
            if (logger==null) 
            {
                logger = new Logger("log_zad3.txt") ;
                return logger;
            }
            else
            {
                return logger;
            }
        }
        
        public void Log(string message)
        {
            try
            {
                using (GetInstance().writer = new StreamWriter(fileName, true))
                {
                    writer.WriteLine(message);
                }
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }

        }
    }
}
