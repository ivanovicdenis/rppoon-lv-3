﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_3_2
{
    class Program_zad2
    {
        
        static void Main(string[] args)
        {
            int[,] matrix = MatrixGenerator.GetInstance().GetRandomMatrix(10, 10);

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 10; j++)
                {
                    Console.Write(matrix[i, j] + ", ");
                }
            }
        }
        
    }
}
