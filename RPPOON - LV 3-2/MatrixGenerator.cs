﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_3_2
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random generator;

        private MatrixGenerator()
        {
            this.generator = new Random();
        }
        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }
        public int[,] GetRandomMatrix(int lowerBound,int upperBound)
        {
            int[,] matrix = new int[lowerBound, upperBound];

            for (int i = 0; i < lowerBound; i++)
            {
                for (int j = 0; j < upperBound; j++)
                {
                    matrix[i, j] = GetInstance().generator.Next(0,2);
                }
            }
            return matrix;
        }
    }

}
