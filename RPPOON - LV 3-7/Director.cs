﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_3_7
{
    class Director
    {
        private IBuilder builder;

        public Director(IBuilder builder)
        {
            this.builder = builder;
        }
        public void ConstructAlertNotification(string author)
        {
            builder
                .SetAuthor(author)
                .SetColor(ConsoleColor.Red)
                .SetLevel(Category.ALERT)
                .SetTime(DateTime.Now)
                .SetTitle("UPOZORENJE")
                .SetText("Hitno napustiti objekat!!");
        }
        public void ConstructErrorNotification(string author)
        {
            builder
                .SetAuthor(author)
                .SetColor(ConsoleColor.DarkBlue)
                .SetLevel(Category.ERROR)
                .SetTime(DateTime.Now)
                .SetTitle("GREŠKA")
                .SetText("Greška u pogonu, potrebno izaći na uvid!!");
        }
        public void ConstructInfoNotification(string author)
        {
            builder
                .SetAuthor(author)
                .SetColor(ConsoleColor.Green)
                .SetLevel(Category.INFO)
                .SetTime(DateTime.Now)
                .SetTitle("INFORMACIJA")
                .SetText("Kraj radnog vremena!");
        }
    }
}
