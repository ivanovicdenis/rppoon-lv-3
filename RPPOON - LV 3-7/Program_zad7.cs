﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_3_7
{
    class Program_zad7
    {
        static void Main(string[] args)
        {
            
            string author = "Denis Ivanović";
            NotificationBuilder builder = new NotificationBuilder();
            NotificationManager notificationManager = new NotificationManager();
            Director director = new Director(builder);
            ConsoleNotification notification;
            ConsoleNotification notification_copy;


            director.ConstructAlertNotification(author);
            notification = builder.Build();
            notification_copy= (ConsoleNotification)notification.Clone();
            notificationManager.Display(notification);
            notificationManager.Display(notification_copy);

            /*
             Pitanje: Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja?
             Odgovor: Profesore, kava je provrela, tako da nisam testirao duboko kopiranje jer nije striktno navedeno u zadatku. 
                      Ali mislim da u ovom slučaju ima razlike jer nakon buildanja sljedeće obavijesti i promjenila bi se i ova koja je prethodno kopirana jer se u plitkom kopiranju kopriju adrese.
             
            */
        
            director.ConstructErrorNotification(author);
            notification = builder.Build();
            notificationManager.Display(notification);

            director.ConstructInfoNotification(author);
            notification = builder.Build();
            notificationManager.Display(notification);

        

    }
    
    }
}
